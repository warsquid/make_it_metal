Make it Metal
==============

Make it Metal is a deep learning model for generating
metal band names, built on top of [textgenrnn](https://github.com/minimaxir/textgenrnn)
and using [Tensorflow.js](https://github.com/minimaxir/textgenrnn)
to run the model in a browser (the model is initially
trained in Python).