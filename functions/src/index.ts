import * as functions from 'firebase-functions';
const admin = require('firebase-admin');
admin.initializeApp();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
exports.name_counter = functions.https.onRequest(
    async (request, response) => {
        await admin.database().ref('counts/names_generated')
            .transaction((currentVal: number) => currentVal + 1);
        response.sendStatus(200);
    }
)

exports.visit_counter = functions.https.onRequest(
    async (request, response) => {
        await admin.database().ref('counts/visitors')
            .transaction((currentVal: number) => currentVal + 1);
        response.sendStatus(200);
    }
)
