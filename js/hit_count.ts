async function fetchAsync (url) {
  let response = await fetch(url);
  let data = await response.json();
  return data;
}

export async function squidcount_add(name: string) {
    let path = `https://squidcount.deta.dev/add/${name}`;
    let resp = await fetchAsync(path);
    return resp;
}

// Hit count on page load.
document.addEventListener('DOMContentLoaded', function() {
    let resp = squidcount_add("make_it_metal");
    resp.then((data) => console.log(data["count"]));
}, false);