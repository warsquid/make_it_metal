// This code is adapted from https://github.com/minimaxir/textgenrnn/,
// released under the MIT license:
//
// MIT License

// Copyright (c) 2017-2020 Max Woolf

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
const tf = require('@tensorflow/tfjs');
import { InputSpec } from '@tensorflow/tfjs-layers/dist/engine/topology';

export class AttentionWeightedAverage extends tf.layers.Layer {
    static className = 'AttentionWeightedAverage';

    constructor(config) {
        super(config);
        this.init = tf.initializers.randomUniform;
        this.supports_masking = true;
        this.return_attention = false;
    }

    build(input_shape) {
        this.inputSpec = [new InputSpec({ndim: 3})];

        this.W = this.addWeight(
            this.name + '_W',
            [input_shape[2], 1],
            undefined,
            this.init,
            undefined,
            true
        );
        super.build(input_shape);
    }

    call(x, mask=null) {
        // Extract input
        x = x[0];
        let logits = tf.dot(x.squeeze(), this.W.read());
        logits = tf.reshape(logits, [x.shape[0], x.shape[1]]);
        let ai = tf.exp(logits.sub(tf.max(logits, 1, true)));

        if (mask != null) {
            if (mask.length > 0) {
                mask = tf.cast(tf.tensor(mask), 'float32');
                ai = ai * mask;
            }
        }

        // Don't have K.epsilon in JS so use a default?
        let default_epsilon = 1e-7;;
        let att_weights = ai.div(tf.sum(ai, 1, true).add(default_epsilon));
        let weighted_input = x.mul(tf.expandDims(att_weights, 2));
        let result = tf.sum(weighted_input, 1);

        if (this.return_attention) {
            combined = [result, att_weights];
            return(combined);
        }
        return(result);
    }

    getOutputShapeFor(input_shape) {
        return(this.compute_output_shape(input_shape));
    }

    computeOutputShape(input_shape) {
        let output_len = input_shape[2];

        if (this.return_attention) {
            out_array = [
                [input_shape[0], output_len],
                [input_shape[0], input_shape[1]]
            ];
            return(out_array);
        }
        return([input_shape[0], output_len])
    }

    computeMask(input, input_mask=null) {
        if (Array.isArray(input_mask)) {
            let empty = new Array(input_mask.length).fill(null);
            return(empty)
        }
        return(null);
    }
}
