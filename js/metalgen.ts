import $ from "jquery";
const mg = require('./metalgen_utils');
const sc = require("./hit_count");
const vocab_obj = require('../data/textgenrnn_vocab.json');
import {AttentionWeightedAverage} from './attention_weighted_average';
const tf = require('@tensorflow/tfjs');
tf.serialization.registerClass(AttentionWeightedAverage);

const N_FONTS = 8;
const FONT_COLOURS = [
    "#d41212",
    "#fff",
    "#be5010",
    "#44258d"
]

const GENRES = require('../data/metalgen_genres.json'); 


// Returns random int between 1 and max (inclusive)
function get_random_int(max) {
  return Math.floor(Math.random() * Math.floor(max)) + 1;
}

function to_title_case(str) {
  return str.toLowerCase().split(' ').map(function (word: string) {
    return (word.charAt(0).toUpperCase() + word.slice(1));
  }).join(' ');
}

function create_genre_tags() {
   GENRES.map((genre) => {
        // Clean regex, if needed
        genre = genre.replace("\\b", "");

        let genre_item = document.createElement("li");
        let genre_input = document.createElement("input")
        $(genre_input).attr("type", "checkbox")
            .attr("value", genre)
            .attr("id", `checkbox-${genre}`); 
        let genre_label = document.createElement("label")
        $(genre_label).attr("for", `checkbox-${genre}`)
            .text(to_title_case(genre));
        genre_item.appendChild(genre_input);
        genre_item.appendChild(genre_label);
        $("#genre_list").append(genre_item);
   });
}

function select_random_genres(select_prob: number = 0.15) {
    let checkboxes = $("input[type=checkbox]");
    checkboxes.prop("checked", false);
    checkboxes.map((index, box) => {
        let dice_roll = Math.random();
        if (dice_roll < select_prob) {
            $(box).prop("checked", true);
        }
    })
}

function get_context_input(): Array<number> {
    let genre_boxes = $("#genre_list li input");
    let context = [];
    genre_boxes.map((index, box) => {
        if ($(box).prop("checked")) {
            context.push(1);
        } else {
            context.push(0);
        }
    });
    return context;
}

function set_band_name(text: string) {
    let font_num = get_random_int(N_FONTS);
    let colour = FONT_COLOURS[get_random_int(FONT_COLOURS.length) - 1];
    $("#band_name_text").attr("class", "band_font" + font_num.toString())
        .css("color", colour)
        .text(text);
}


$("#randomize_genres").on("click", function() {select_random_genres()});

$(async function() {
    $("#band_name").hide();
    create_genre_tags();
    $("#page_loader_icon").fadeOut(4000);
    $(".page_loader").fadeOut(4000);

    let model = await mg.load_model('/model/model.json');
    let vocab = await mg.object_to_map(vocab_obj);

    $("#generate_button").on("click", async function() {
        $("#loading_icon").addClass("loading");
        $("#loading_container").show();
        $(this).prop("disabled", true);
        $("#band_name").hide();

        let hit_count = sc.squidcount_add("make_it_metal_generate");
        hit_count.then((data) => {
            console.log(`${data["count"]} band names have been generated so far :)`);
        });

        let context = get_context_input();
        let temperature = $("#temperature_slider").val() / 100;
        // Need to run the slow parts in setTimeout for show/hide to work
        //   properly
        setTimeout(async function() {
            let generated = await mg.metalgen_generate(
                model,
                vocab,
                context,
                temperature
            )
            set_band_name(generated);
            $("#generate_button").prop("disabled", false);

            $("#loading_container").fadeOut("medium", () => {
                $("#band_name").show();
            });
        });
    })
});