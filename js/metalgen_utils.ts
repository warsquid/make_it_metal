const tf = require('@tensorflow/tfjs');
const request = require('request');

const EPSILON = 1e-7;

export async function load_model(url) {
    const model = await tf.loadLayersModel(url, false);
    return model;
}


export async function load_vocab(url: string): Promise<Map<string, string>> {
    let vocab_obj = require(url);
    let vocab = object_to_map(vocab_obj);
    return vocab;
}

export function object_to_map(obj): Map<string, any> {
    let keys = Object.keys(obj);
    let map = new Map();
    keys.map(k => map.set(k.toString(), obj[k]));
    return map;
}

function make_context_input(genres, all_genres) {
    return all_genres.map(g => genres.includes(g) ? 1 : 0);
}

async function random_context(context_length, prob: number = 0.1) {
    return Array(context_length).map(x => Math.random() < prob);
}

export function encode_sequence(text: Array<string>, vocab: Map<string, number>, maxlen) {
    let encoded = text.map(x => vocab.get(x) || 0);
    // Make sure we pass an array of arrays
    return pad_sequences([encoded], maxlen)[0];
}

export function pad_sequences(sequences, maxlen) {
    return sequences.map(seq => {
        if (seq.length >= maxlen) {
            return seq;
        }
        let n_to_add = maxlen - seq.length;
        return Array(n_to_add).fill(0).concat(seq);
    })
}

export async function sample_chars(pred_probs: Array<number>, temperature: number) {
    let normed_probs = pred_probs.map(x => Math.log(x + EPSILON) / temperature);
    let sample_ok = false;
    let sample;
    while (! sample_ok) {
        let sample_result = await tf.multinomial(normed_probs, 1).array();
        sample = sample_result[0];
        // Don't allow placeholder of 0
        if (sample != 0) {
            sample_ok = true;
        }
    }
    return sample;
}

function reverse_map(m: Map<any, any>) {
    let rev_map = new Map();
    m.forEach((value, key) => rev_map.set(value, key));
    return rev_map;
}

export async function metalgen_generate(
    model: tf.LayersModel,
    vocab: Map<string, number>,
    context_input: Array<number>,
    temperature: number = 0.5,
    maxlen: number = 40,
    meta_token: string = '<s>') {

    let num_to_char = reverse_map(vocab);
   
    let text = [meta_token];

    let end = false;
    while (! end) {
        let start_index = Math.max(text.length - maxlen, 0);
        let encoded_text = encode_sequence(text.slice(start_index), 
                                           vocab, 
                                           maxlen);
        let combined_input = [tf.tensor2d(encoded_text, [1, encoded_text.length]), 
                              tf.tensor2d(context_input, [1, context_input.length])];
        let prediction = model.predict(combined_input, {batchSize: 1});
        let char_probs = await prediction[1].array();
        let next_index = await sample_chars(char_probs[0], temperature);
        let next_char = num_to_char.get(next_index);
        text.push(next_char);

        let is_meta = next_char == meta_token;
        let is_long_enough = text.length >= maxlen - 1;
        if (is_meta || is_long_enough) {
            end = true;
        }
    }

    return text.filter(x => x != meta_token).join('');
}